from re import compile
from typing import Optional, Tuple


class Address:
    PATTERN_HEADER = compile("(?P<name>.*) <(?P<addr>.*@.*)>")

    def __init__(self, name: Optional[str], addr: Optional[str] = None):
        if addr is None:
            name, addr = self.parse_smtp_header(name)
        self.name = name
        self.addr = addr

    @staticmethod
    def parse_smtp_header(header: str) -> Tuple[Optional[str], str]:
        match = Address.PATTERN_HEADER.match(header)
        return (match["name"], match["addr"]) if match else (None, header)

    @staticmethod
    def from_smtp_header(header: str):
        return Address(*Address.parse_smtp_header(header))

    def __str__(self):
        return f"{self.name} <{self.addr}>" if self.name else self.addr


NOREPLY = Address(None, "")
