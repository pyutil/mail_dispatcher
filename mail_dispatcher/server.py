import smtplib


class SmtpServer:
    def __init__(self, auth):
        self.auth = auth

    def connect(self) -> smtplib.SMTP:
        session = smtplib.SMTP(self.auth.host, self.auth.port)
        session.ehlo()
        session.starttls()
        session.ehlo()
        session.login(self.auth.username, self.auth.password)
        return session

    @property
    def session(self) -> smtplib.SMTP:
        try:
            return self.connect()
        except smtplib.SMTPAuthenticationError:
            self.auth.refresh()
            return self.connect()
