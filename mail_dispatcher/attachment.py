from email.encoders import encode_base64
from email.mime.application import MIMEApplication
from email.mime.audio import MIMEAudio
from email.mime.base import MIMEBase
from email.mime.image import MIMEImage
from email.mime.text import MIMEText
from io import BytesIO, FileIO, IOBase, StringIO
from mimetypes import guess_type
from typing import Optional, Tuple, Type, Union


class Attachment:
    class MIMEFallback(MIMEBase):
        def __init__(
            self,
            payload,
            _maintype: str = "application",
            _subtype: str = "octet-stream",
            _encoder=encode_base64,
        ):
            super().__init__(_maintype, _subtype)
            self.set_payload(payload)
            _encoder(self)

    def __init__(self, attachment: Union[IOBase, str], encoder=encode_base64):
        self.data = attachment
        self.encoder = encode_base64

    @classmethod
    def open(cls, path: str):
        content_type, encoding = guess_type(path)
        if content_type is None or encoding is not None:
            return open(path, "rb")
        if content_type.startswith("text"):
            return open(path, "r")
        return open(path, "rb")

    @classmethod
    def infer_stream_properties(
        cls, stream: IOBase
    ) -> Tuple[Tuple[str, Optional[str]], Type[MIMEBase]]:
        typecheck = stream
        while True:
            for fieldname in ("buffer", "raw"):
                if hasattr(typecheck, fieldname):
                    typecheck = getattr(typecheck, fieldname)
                    break
            else:
                break
        if isinstance(typecheck, StringIO):
            return ("text", "plain"), MIMEText
        if isinstance(typecheck, BytesIO):
            return ("application", "octet-stream"), MIMEApplication
        if isinstance(typecheck, FileIO):
            content_type, encoding = guess_type(stream.name)
            if content_type is None or encoding is not None:
                content_type = "application/octet-stream"
            content_type = content_type.split("/", 1)
            if content_type[0] == "text":
                content_wrapper = MIMEText
            elif content_type[0] == "image":
                content_wrapper = MIMEImage
            elif content_type[0] == "audio":
                content_wrapper = MIMEAudio
            else:
                content_wrapper = cls.MIMEFallback
            return content_type, content_wrapper
        raise TypeError("needs an IO object")

    @property
    def data(self):
        try:
            self.seek(0)
        except Exception:
            pass
        kwargs = {} if self.__cwrap__ is MIMEText else {"_encoder": self.encoder}
        return self.__cwrap__(self.read(), _subtype=self.__ctype__[1], **kwargs)

    @data.setter
    def data(self, value: Union[IOBase, str]):
        if isinstance(value, str):
            value = self.open(value)
        self.__stream__ = value
        self.__ctype__, self.__cwrap__ = self.infer_stream_properties(value)

    @property
    def content_type(self):
        return "/".join(self.__ctype__)

    def __getattr__(self, attribute: str):
        return getattr(self.__stream__, attribute)
