from email.mime.multipart import MIMEMultipart
from io import IOBase
from typing import List, Tuple, Union

from .address import Address
from .attachment import Attachment
from .server import SmtpServer


class Mail:
    def __init__(self, smtp: SmtpServer, sender_addr: str, message: MIMEMultipart):
        self._smtp = smtp
        self._sender = sender_addr
        self.message = message
        self._orig_payload = message.get_payload()
        self.attachments = []

    def get_attachment_data(self, ignore_unnamed: bool = False):
        output = []
        attachment_index = 0
        for attachment in self.attachments:
            data = attachment.data
            if getattr(attachment, "name", None):
                name = attachment.name
            else:
                if ignore_unnamed:
                    continue
                attachment_index += 1
                name = f"file_{attachment_index}"
            data.add_header("Content-Type", attachment.content_type)
            data.add_header("Content-Disposition", "attachment", filename=name)
            data.add_header("Content-ID", "<{name}>")
            output.append(data)
        return output

    def add_attachment(
        self, attachment: Union[Attachment, str, IOBase], name: str = None
    ):
        if not isinstance(attachment, Attachment):
            attachment = Attachment(attachment)
        if name:
            attachment.name = name
        self.attachments.append(attachment)

    @classmethod
    def _parse_receivers(
        cls, receivers: Union[str, Address, List[Union[str, Address]]]
    ) -> Tuple[str, List[str]]:
        if not isinstance(receivers, list):
            receivers = [receivers]
        # receivers = [Address(receiver) for receiver in receivers]
        # return ", ".join(str(receiver) for receiver in receivers), [receiver.addr for receiver in receivers]
        return ", ".join(str(receiver) for receiver in receivers), [
            (receiver.addr if isinstance(receiver, Address) else receiver)
            for receiver in receivers
        ]

    def send(self, receivers: Union[str, Address, List[Union[str, Address]]]):
        del self.message["To"]
        self.message["To"], receivers = self._parse_receivers(receivers)
        self.message.set_payload([*self._orig_payload, *self.get_attachment_data()])
        with self._smtp.session as session:
            session.sendmail(self._sender, receivers, self.message.as_string())
