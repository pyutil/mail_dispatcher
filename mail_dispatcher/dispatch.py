from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from typing import Optional, Tuple, Type, Union

from .address import NOREPLY, Address
from .mail import Mail
from .server import SmtpServer
from .templates import DefaultTemplatingEngine, TemplatingEngine


class MailDispatcher:
    def __init__(
        self,
        auth,
        sender: Union[str, Tuple[Optional[str], str], Address],
        reply_to: Optional[Union[str, Tuple[Optional[str], str], Address]] = None,
        templating_engine: Type[TemplatingEngine] = DefaultTemplatingEngine,
    ):
        self._smtp = SmtpServer(auth)
        self.sender = sender
        self.reply_to = reply_to
        self._templating_engine = templating_engine()

    @property
    def sender(self):
        return self._sender

    @sender.setter
    def sender(self, sender_data: Union[str, Tuple[Optional[str], str], Address]):
        if isinstance(sender_data, Address):
            self._sender = sender_data
        elif isinstance(sender_data, str):
            self._sender = Address(sender_data)
        else:
            self._sender = Address(*sender_data)

    @property
    def reply_to(self):
        return self._reply_to if self._reply_to else self.sender

    @reply_to.setter
    def reply_to(
        self,
        reply_to_data: Optional[Union[str, Tuple[Optional[str], str], Address]],
    ):
        if reply_to_data is None:
            self._reply_to = None
        elif isinstance(reply_to_data, Address):
            self._reply_to = reply_to_data
        elif isinstance(reply_to_data, str):
            self._reply_to = Address(reply_to_data)
        else:
            self._reply_to = Address(*reply_to_data)

    def mail_from_string(
        self, subject: str, template: str, context: dict = {}, syntax: str = "plain"
    ):
        message = MIMEMultipart()
        message["Subject"] = subject
        message["From"] = str(self.sender)
        message["Reply-To"] = str(self.reply_to)
        message.attach(
            MIMEText(self._templating_engine.render(template, context), _subtype=syntax)
        )
        return Mail(self._smtp, self.sender.addr, message)

    def mail_from_file(
        self,
        subject: str,
        template_path: str,
        context: dict = {},
        syntax: str = "plain",
        **open_kwargs,
    ):
        with open(template_path, "r", **open_kwargs) as file:
            return self.mail_from_string(subject, file.read(), context, syntax)
