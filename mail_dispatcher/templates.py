from abc import ABC, abstractmethod


class TemplatingEngine(ABC):
    """Basic interface for template rendering.

    Methods:
        render  @abstract
    """

    @abstractmethod
    def render(self, template: str, context: dict = {}) -> str:
        raise NotImplementedError("abstract method needs override")


class DefaultTemplatingEngine(TemplatingEngine):
    """Templating engine utilising str.format().

    Methods:
        render  @override
    """

    def render(self, template: str, context: dict = {}) -> str:
        return template.format(**context)


class JinjaTemplatingEngine(TemplatingEngine):
    """Templating engine utilising jinja2.Template.render().

    Internally imports jinja2.Template, thus jinja2 must be
    installed to avoid import errors.

    Fields:
        _jinja  type    internal reference to jinja.Template class

    Methods:
        render  @override
    """

    def __init__(self):
        from jinja2 import Template

        self._jinja = Template

    def render(self, template: str, context: dict = {}) -> str:
        template = self._jinja(template)
        return template.render(**context)
