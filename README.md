# Simple e-mail dispatching actor
## Maintainer: Aachne Dirkschneider <n45t31@protonmail.com>
## Site: <https://gitlab.com/pyutil/mail_dispatcher>

### Table of Contents

- [structure](#project-structure)
- [usage](#usage)
  - [auth](#request-authorizers)
  - [templates](#templating-engines)
  - [identity](#identity-headers)
  - [dispatch](#message-dispatch)
  - [examples](#code-samples)
    - [templates](#templating)
    - [identity](#identity)
    - [dispatch](#dispatch)
- [tests](#testing-library-functionality)
  - [unit](#unit-tests)
  - [integration](#integration-tests)

### Project structure

```
-address
 +-Address
 | +-parse_smtp_header(staticmethod)
 | +-from_smtp_header(staticmethod)
 +-NOREPLY
-attachment
 +-Attachment
   +-MIMEFallback(email.mime.base.MIMEBase)
   +-open(classmethod)
   +-infer_stream_properties(classmethod)
   +-data(property)
   +-content_type(property)
-dispatch
 +-MailDispatcher
   +-sender(property)
   +-reply_to(property)
   +-mail_from_string
   +-mail_from_file
-mail
 +-Mail
   +-get_attachment_data
   +-add_attachment
   +-send
-server
 +-SmtpServer
   +-connect
   +-session(property)
-templates
 +-TemplatingEngine(abc.ABC)
 | +-render(abstractmethod)
 +-DefaultTemplatingEngine(TemplatingEngine)
 | +-render
 +-JinjaTemplatingEngine(TemplatingEngine)
   +-render
```

### Usage

Main package functionality revolves around **dispatch.MailDispatcher** instances. Each instance
accepts an authentication object, sender address and an optional argument specifying template
engine to use while rendering message content.

#### Request authorizers

Given that majority of SMTP services require their clients to authenticate submitted requests, any object adhering 
to SmtpAuth interface must expose `refresh()` method and *host*, *port*, *username* and *password* properties.

#### Templating engines

Message text content is generated from a template and context that carries variable values. Every
such pair is passed to a templating engine whose `render(template,context)` method handles context
injection and returns filled template. Two standard engines: *str.format*-based and *jinja.Template.render*-based
are provided by *templates* module - with *templates.JinjaTemplatingEngine* requiring *jinja2* for
instantiation.

#### Identity headers

Every **dispatch.MailDispatcher** instance must be provided with *AT LEAST* a sender address to assign to 
delegated message's *From* header. To satisfy <https://tools.ietf.org/html/rfc822> both `sample@email.domain`
and `Sample Name <sample@email.domain>` are supported; *sender* property accepts them as a **str**, a  **tuple<[str],str>**
or a direct instantiation of **address.Address**. For messages that differentiate between sender and reply-to addresses, 
a separate *reply_to* property exists that additionally accepts *None* (equivallent to `*.reply_to = *.sender`)
and *address.NOREPLY* that sets an empty header and will be utilised further when IETF introduces proper
noreply headers.

#### Message dispatch

**dispatch.MailDispatcher** instancess expose `mail_from_string` and `mail_from_file` methods that delegate
proxy **mail.Mail** objects that can be expedited using `send` method, which handles SMTP session initialization and authentication. 
Both `mail_from` methods accept message subject, a parameter specifying message template (either a string or a system path) and 
message variables context. **mail.Mail::send** method accepts a list of receivers defined as strings (simple addresses) or
**address.Address** objects.

#### Code samples

##### templating

```python3
# THIS EXAMPLE DEMONSTRATES TEMPLATING ENGINE CREATION

from io import StringIO
from mail_dispatcher.templates import TemplatingEngine

class MyTemplatingEngine(TemplatingEngine):
    def render(self,template:str,context:dict={})->str:
        with StringIO() as out:
            for token in template.split():
                if token.startswith('@'):
                    out.write(str(context[token[1:]]))
                else:
                    out.write(token)
                out.write(' ')
            return out.getvalue()[:-1]

template = "test No. @i"
engine = MyTemplatingEngine()
for test_id in range(5):
    print(
        engine.render(
            template,
            {
                'i' : test_id,
            }
        )
    )
     
# expected output:
# STDOUT:
#  test No. 0
#  test No. 1
#  test No. 2
#  test No. 3
#  test No. 4 
```

##### identity

```python3
# THIS EXAMPLE DEMONSTRATES IDENTITY HEADERS MANIPULATION

from mail_dispatcher import MailDispatcher, NOREPLY

md = mail_dispatcher(None,'sample@email.domain')

print(md.sender)
print(md.reply_to)

# expected output:
# stdout:
#  sample@email.domain
#  sample@email.domain

md.sender = 'sample name <sample@email.domain>'
md.reply_to = 'other@email.domain'

print(md.sender)
print(md.reply_to)

# expected output:
# STDOUT:
#  sample name <sample@email.domain>
#  other@email.domain

md.sender = (None,'other@email.domain')
md.reply_to = NOREPLY

print(md.sender)
print(md.reply_to)

# expected output:
# STDOUT:
#  other@email.domain
#  

md.sender = 'sample@email.domain'
md.reply_to = None

print(md.sender)
print(md.reply_to)

# expected output:
# STDOUT:
#  sample@email.domain
#  sample@email.domain
```

##### dispatch

```python3
# THIS EXAMPLE DEMONSTRATES BASIC MAIL DISPATCHER CONFIGURATION

from mail_dispatcher import MailDispatcher, Address

auth = ...

md = MailDispatcher(
    auth=auth,
    sender='sender@domain.com',
)

mail = md.mail_from_string("test message","test message")
mail.send("receiver@domain.com")

with open("test-message.txt","w") as file:
    file.write("test message No. {message_id}")

md.mail_from_file(
    "test message",
    "test-message.txt",
    {
        "message_id":1,
    }
).send(
    [
        "receiver@domain.com",
        Address("Sample Name", "sample@email.domain"),
    ]
)

```

```python3
# THIS EXAMPLE DEMONSTRATES MAIL DISPATCH WITH CUSTOM AUTH, SENDER ADDRESS AND JINJA TEMPLATE

from os import environ

from mail_dispatcher import MailDispatcher
from mail_dispatcher.templates import JinjaTemplatingEngine

class MyAuth:
    def __init__(self):
        self.refresh()

    def refresh(self):
        self.host = environ["SMTP_HOST"]
        self.port = environ["SMTP_PORT"]
        self.username = environ["SMTP_USER"]
        self.password = environ["SMTP_PASS"]

md = MailDispatcher(
    MyAuth(),
    "custom@sender.com",
    templating_engine=JinjaTemplatingEngine,
)

mail = md.mail_from_string(
    "test message",
    "test message No. {{ message_id }}",
    {
        "message_id" : 1,
    }
)
for receiver_id in range(4):
    mail.send(f"receiver{receiver_id}@example.com")
    
```

#### Testing library functionality

##### Unit tests

Format: None

##### Integration tests

Format: None
